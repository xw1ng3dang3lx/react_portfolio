const webpackDevServer = require('webpack-dev-server');
const webpack = require('webpack');

const config = require('./webpack.config.js');
const options = {
  historyApiFallback: true,
  hot: true,
  host: 'localhost'
};

const optimization = {
  optimization: {
    minimize: false
  }
};

const serverConfig = Object.assign({}, config, optimization);

webpackDevServer.addDevServerEntrypoints(serverConfig, options);
const compiler = webpack(config);
const server = new webpackDevServer(compiler, options);

server.listen(5000, 'localhost', () => {
  console.log('dev server listening on port 5000');
});
