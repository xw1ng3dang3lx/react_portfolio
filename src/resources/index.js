import {en_text} from "./en";

export const getText = (lang) => {
    if (lang === "en") {
        return en_text;
    }
}
