import React from "react";
import {render} from "react-dom";

import * as types from "./constants/ActionTypes";

/* Views */
import App from "./App";

/* Redux + React-Router Compatibility */
import { routerReducer } from "react-router-redux";

/* State Container */
import { createStore, combineReducers, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import thunkMiddleware from "redux-thunk";
import * as reducers from "./reducers/index.js"

// Reducers
const reducer = combineReducers({
  ...reducers,
  routing: routerReducer
});
const store = createStore(
  reducer,
  applyMiddleware(thunkMiddleware)
);

const screenResize = ({width, height}) => {
    return {
        type: types.SCREEN_RESIZE,
        screenWidth: width,
        screenHeight: height
    };
}

window.addEventListener("resize", () => {
    store.dispatch(screenResize({width: window.innerWidth, height: window.innerHeight}));
});

render((
    <Provider store={store}>
      <App />
    </Provider>)
  ,
  document.getElementById("root")
);

if (module.hot) {
  module.hot.accept();
}
