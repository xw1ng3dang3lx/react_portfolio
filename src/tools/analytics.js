

// ReactGA.timing({
//   label: "TimingAnalytics",
//   category: "ScreenAnalytics",
//   variable: "load",
//   value: JSON.stringify(getTimingPerf())
// });

export function getMemoryPerf () {
  const memInfo = window.performance.memory;
  const memory = {
    jsHeapSizeLimit: memInfo.jsHeapSizeLimit,
    totalJSHeapSize: memInfo.totalJSHeapSize,
    usedJSHeapSize: memInfo.usedJSHeapSize,
    percentagedUsed: memInfo.usedJSHeapSize/memInfo.totalJSHeapSize * 100
  }
  const memKeys = Object.keys(memory);
  for (let i = 0; i < memKeys.length; i++) {
    ReactGA.timing({
      label: "MemoryAnalytics",
      category: "MemoryAnalytics",
      variable: memKeys[i],
      value: memory[memKeys[i]]
    });
  }

}
export function getTimingPerf () {
  const timing = window.performance.timing;
  return timing;
}

export function getScreenAnalytics () {
  const screenInfo = {
    screenWidth: screen.width,
    screenHeight: screen.height,
    orientation: screen.orientation.type,
    colorDepth: screen.colorDepth,
    pixelDepth: screen.pixelDepth,
    viewingWidth: window.innerWidth,
    viewingHeight: window.innerHeight
  }
  const screenKeys = Object.keys(screenInfo);
  for (let i = 0; i < memKeys.length; i++) {
    ReactGA.timing({
      label: "ScreenAnalytics",
      category: "ScreenAnalytics",
      variable: screenKeys[i],
      value: screenInfo[screenKeys[i]]
    });
  }
}
