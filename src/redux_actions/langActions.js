import * as types from "../constants/ActionTypes"

const convertLang = (lang) => {
    if (lang.indexOf("en-") === 0) {
        return "en";
    }
    else {
        return "en";
    }
}

export const setLang = () => {
    const language =  navigator.language || navigator.userLanguage || window.navigator.language;
    const convertedLang = convertLang(language);
    return ({ type: types.SET_LANG, lang: convertedLang});
}
