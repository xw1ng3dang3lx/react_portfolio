import React, {Component} from "react";
import LargeProject from "./components/LargeProject";
import SmallProject from "./components/SmallProject";

import { Parallax } from "react-parallax";

import classNames from "classnames";

import {connect} from "react-redux";

import {projects} from "../ProjectPage/configs";

/* CSS */
import "./styles.scss";
import "../../css/parallax.scss";

export class My_Projects extends Component {
    constructor(props) {
        super(props);
    }

    getSmallProjectsContainer = () => {
      const smallProjects = projects.filter(proj => proj.show && !proj.largeCard);
      if (smallProjects.length === 0) {
          return null;
      }
      let segmentedProjects = [],
        tempProjects = [];
      smallProjects.forEach((project, idx) => {
        tempProjects.push(project);

        if ((idx + 1) % 3 === 0 || (idx + 1 === smallProjects.length)) {
          segmentedProjects.push(tempProjects);
          tempProjects = [];
        }
      });

      return segmentedProjects.map((projects) => {
          const smallProjects = projects.map(project => {
            return (
                <SmallProject
                  key={project.pid}
                  project={project}
                  classes={projects.length === 3 ? "col-33" : "col-50"}
                />
            );
          });
          return (
            <div className="projectSmallCardsContainer">
              {smallProjects}
            </div>
          );
      });
    }

    render() {
        return (
            <div className="col-100">
                <Parallax bgImage="/images/projectbanner.png" strength={400}>
                    <h1 className="center"> Projects </h1>
                </Parallax>
                {
                    projects.filter(proj => proj.show && proj.largeCard).map((project, index) => {
                        return (
                            <LargeProject
                              screenWidth={this.props.windowState.screenWidth}
                              key={project.pid}
                              project={project}
                              isLeftSide={ index % 2 === 0 }
                            />
                        );
                    })
                }
                {this.getSmallProjectsContainer()}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        windowState: state.windowReducer
    }
}
export default connect(mapStateToProps, null)(My_Projects);
