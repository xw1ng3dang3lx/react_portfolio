import React, {Component} from "react";
import classNames from "classnames";
import ProjectDescription from "../ProjectDescription";
import ProjectImg from "../ProjectImg";

import { Link } from "react-router-dom";

export class LargeProject extends Component {
    constructor(props) {
        super(props);

    }

    getProjectDescription = () => {
        return (
            <ProjectDescription
                screenWidth={this.props.screenWidth}
                project={this.props.project}
            />
        );
    }

    getProjectImg = () => {
        return (
            <ProjectImg
                project={this.props.project}
            />
        );
    }

    render() {
        const projectImg = this.getProjectImg(),
            projectDesc = this.getProjectDescription(),
            {isLeftSide} = this.props;

        let firstDiv, secondDiv;
        if (this.props.screenWidth > 780) {
            firstDiv = isLeftSide  ? projectDesc : projectImg;
            secondDiv = isLeftSide ? projectImg : projectDesc;
        } else {
            firstDiv = projectDesc;
        }

        return (
            <div className="projectCardsContainer">
                <div className="projectCard largeProject">
                    {firstDiv}
                    {secondDiv}
                </div>
            </div>
        );
    }
}

export default LargeProject;
