import React, {Component} from "react";
import ProjectDescription from "../ProjectDescription";
import { connect } from "react-redux"
import classNames from "classnames";

import ReactGA from "react-ga";
import "./styles.scss";

class SmallProject extends Component {
  constructor(props) {
    super(props);
  }

  handleVisitClick = () => {
      const {project} = this.props;
      ReactGA.event({
        category: "SmallProjectCard",
        action: "visited",
        label: project.link
      });

  } 

  render = () => {
    const {classes, nls, project} = this.props,
      cn = classNames(classes, {
        projectCard: true,
        smallProject: true,
      });

    return (
        <div className={cn}>
          <h1 className={classNames({"center": this.props.screenWidth < 780})}>
              {nls[project.title]}
          </h1>
          {project.thumbnail &&
              <img src={project.thumbnail} alt="project_img" className={classNames({"hide": this.props.screenWidth > 780})} />
          }
          <p>{nls[project.description]}</p>
          {project.link &&
            <a href={project.link} onClick={this.handleVisitClick}>{nls.visit}</a>
          }
 
        </div>
    );
  }
}

SmallProject.defaultProps = {
  classes: ""
};

const mapStateToProps = (state) => {
    return {
        nls: state.langReducer.nls
    };
}

export default connect(mapStateToProps, null)(SmallProject);
