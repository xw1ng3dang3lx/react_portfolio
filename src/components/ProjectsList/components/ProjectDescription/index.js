import React, {Component} from "react";
import classNames from "classnames";

import { connect } from "react-redux"
import { withRouter } from "react-router-dom";

import ReactGA from "react-ga";

export class ProjectDescription extends Component {
    constructor(props) {
        super(props);

    }

    handleVisitClick = () => {
      const {project} = this.props;
      ReactGA.event({
        category: "ProjectCard",
        action: "visited",
        label: project.link
      });
    }

    handleMoreInfoClick= () => {
      const {history, project} = this.props,
        moreInfoLink = "/project/" + project.pid;

      ReactGA.event({
        category: "ProjectCard",
        action: "moreInfo",
        label: moreInfoLink
      });

      history.push(moreInfoLink);
    }

    render() {
        const {nls, project} = this.props;

        return (
            <div className="col-50">
                <h1 className={classNames({"center": this.props.screenWidth < 780})}>
                    {nls[project.title]}
                </h1>
                {project.thumbnail &&
                    <img src={project.thumbnail} alt="project_img" className={classNames({"hide": this.props.screenWidth > 780})} />
                }
                <p>{nls[project.description]}</p>
                <div className="projectRedirectContainers">
                    {project.link &&
                        <a href={project.link} onClick={this.handleVisitClick}>{nls.visit}</a>
                    }
                    {project.largeCard &&
                        <button onClick={this.handleMoreInfoClick}>{nls.moreInfo}</button>
                    }
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        nls: state.langReducer.nls
    };
}

export default connect(mapStateToProps, null)(withRouter(ProjectDescription));
