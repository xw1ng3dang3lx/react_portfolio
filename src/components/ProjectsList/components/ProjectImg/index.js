import React, {Component} from "react";
import classNames from "classnames";

import "./styles.scss";

export class ProjectImg extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        const project = this.props.project;

        return (
            <div className="col-50">
                <img src={project.thumbnail} alt="project_img"></img>
            </div>
        );
    }
}

export default ProjectImg;
