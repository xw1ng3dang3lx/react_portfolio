import React, {Component} from "react";
import { Link } from "react-router-dom";

import BigTitle from "../common/BigTitle/";

import classNames from "classnames";
import LazyLoad from "react-lazyload";
import Waypoint from "react-waypoint";

import {connect} from "react-redux";

/* CSS */
import "./about_me.css";

/* Analytics */


const initialStats = [
  {
    percent: 15,
    curPxHeight: 0
  },
  {
    percent: 10,
    curPxHeight: 0
  },
  {
    percent: 25,
    curPxHeight: 0
  },
  {
    percent: 40,
    curPxHeight: 0
  },
  {
    percent: 10,
    curPxHeight: 0
  }
];

const mapStateToProps = (state) => {
  return {
    windowState: state.windowReducer
  }
}

const mapDispatchToProps = (dispatch => {
  return {
  }
});


class About_Me extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imgHeight: 0,
      animationInterval: null,
      animationIndex: 0,
      myStats: initialStats,
      hasAnimated: false
    }
  }
  componentDidMount() {
  }

  componentWillUnmount = () => {
    if (this.state.animationInterval) {
      clearInterval(this.state.animationInterval);
    }
  }

  animateLegoBoy = (height, index) => {
    const animatePiece = () => {
      const index = index || this.state.animationIndex;
      const imgHeight =  height;
      const myStats = this.state.myStats;
      if (index < myStats.length) {
        const percent = myStats[index].percent/100;
        const maxHeight = imgHeight * percent;
        const curPxHeight = myStats[index].curPxHeight;
        if (curPxHeight < maxHeight) {
          myStats[index].curPxHeight = curPxHeight + 2;
          this.setState({myStats: myStats})
        }
        else {
          this.setState({myStats: myStats, animationIndex: index +1 })
        }
      }
      else {
        window.clearInterval(this.state.animationInterval);
      }
    }
    const interval = window.setInterval(animatePiece.bind(null), 5);
    this.setState({animationInterval: interval})
  }

  reRenderMe = () => {
    this.setState({animationIndex: 0, myStats: initialStats});
  }

  _handleWaypointEnter = () => {
    if (!this.state.hasAnimated) {
      const height = document.getElementById("legoboi").clientHeight;
      const width = document.getElementById("legoboi").width;
      this.setState({ imgHeight: height, imgWidth: width, hasAnimated: true });
      this.animateLegoBoy(height);
    }

  }

  render = () => {
    const myStats = this.state.myStats;

    const imgHeight = (this.props.windowState.screenWidth <=  568 ? 280 : 480);
    const imgWidth = (this.props.windowState.screenWidth <=  568 ? 180: 500 );

    const legoShade1 = {
      backgroundColor: "#d2190b",
      height : myStats[0].curPxHeight +"px",
      bottom: 0,
      width: imgWidth+"px"
    };
    const legoShade2 = {
      backgroundColor: "#ea1c0d",
      height : myStats[1].curPxHeight +"px",
      bottom: myStats[0].curPxHeight +"px",
      width: imgWidth+"px"
    };
    const legoShade3 = {
      backgroundColor: "#F44336",
      height : myStats[2].curPxHeight +"px",
      bottom: (myStats[1].curPxHeight + myStats[0].curPxHeight) +"px",
      width: imgWidth+"px"
    };
    const legoShade4 = {
      backgroundColor: "#f55a4e",
      height : myStats[3].curPxHeight +"px",
      bottom: (myStats[2].curPxHeight + myStats[1].curPxHeight + myStats[0].curPxHeight) +"px",
      width: imgWidth+"px"
    };
    const legoShade5 = {
      backgroundColor: "#EDA39D",
      height : myStats[4].curPxHeight +"px",
      bottom: (myStats[3].curPxHeight + myStats[2].curPxHeight + myStats[1].curPxHeight + myStats[0].curPxHeight) +"px",
      width: imgWidth+"px"
    };

    const legoContainerStyle = {
      height: imgHeight + "px"
    }

    const shadeContainerStyle = {
      width: imgWidth + "px"
    };
    return (
      <div className="col-100 centerContainer about_me_section">
        <div className="col-80 centeredCol">
          <BigTitle
            center={true}
            title="About Me"
          />
          <div className="my_information">
            <h2>Hello! I am a Software Engineer with a passion for designing beautiful and functional experiences. I specialize in Front End Development and UI/UX design. </h2>
          </div>
          <Waypoint
            onEnter={this._handleWaypointEnter}
          />
          <div id="lego" className="lego-me" style={legoContainerStyle}>
            <LazyLoad height={500} offset={1000}>
              <img id="legoboi" src="/images/legome.png" alt="legoboi"></img>
            </LazyLoad>
            <div className="lego-container" style={shadeContainerStyle}>
              <div className={"legoShade"} style={legoShade1}></div>
              <div className={"legoShade"} style={legoShade2}></div>
              <div className={"legoShade"} style={legoShade3}></div>
              <div className={"legoShade"} style={legoShade4}></div>
              <div className={"legoShade"} style={legoShade5}></div>
            </div>
            <div className="label-container" style={legoContainerStyle}>
              <div id="left-skills-outer">
                <div id="left-skills-inner" style={{"right": imgWidth/2 + "px"}}>
                  <div className="label4">
                    <h2>40%</h2>
                    <h2>ReactJS</h2>
                    <h2>AngularJS</h2>
                  </div>
                  <div className="label5">
                    <h2>10%</h2>
                    <h2>Coffee</h2>
                  </div>
                </div>
              </div>
              <div id="right-skills-outer">
                <div id="right-skills-inner" style={{"left": imgWidth/2 + "px"}}>
                  <div className="label1">
                    <h2>10% Sith Lord</h2>
                  </div>
                  <div className="label2">
                    <h2>25% HTML</h2>
                    <h2>CSS</h2>
                    <h2>JS</h2>
                  </div>
                  <div className="label3">
                    <h2>15% Java</h2>
                  </div>
                </div>
              </div>

            </div>
          </div>

          <div className="resume_download_ctr">
            <a href="/resume.pdf" className="transparentTextBtn blackText centered">Resume</a>
          </div>

        </div>
      </div>
    );
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(About_Me);
