import React, {Component} from "react";
import { Link } from "react-router-dom";

import BigTitle from "../common/BigTitle/";

import classNames from "classnames";
import LazyLoad from "react-lazyload";

import {connect} from "react-redux";

/* CSS */
import "./styles.scss";

/* Analytics */


const mapStateToProps = (state) => {
    return {
        windowState: state.windowReducer
    }
}

class About_Me extends Component {
    constructor(props) {
        super(props);
        this.state = {
            textBubble: "",
            textIndex: 0,
            phraseIndex: 0,
            delayForNextSentence: 0,
            numOfDots: 0,
            imgsReceived: []
        };

        const phrases =
        [
            "Hello!",
            "I am a Software Engineer with a passion for designing beautiful and functional experiences.",
            "I specialize in Front End Development and UI/UX design.",
            "Check out some of the cool stuff I've done below."
        ];
        this.phrases = phrases.map((phrase) => {
            return phrase.split(" ");
        });

    }

    componentWillUnmount = () => {
        if (this.textInterval) {
            clearInterval(this.textInterval);
        }
    }

    fireAnimation = () => {
        this.textInterval = setInterval(() => {
            if (this.state.phraseIndex < this.phrases.length) {
                const {phraseIndex, textIndex, numOfDots, delayForNextSentence} = this.state;
                const curPhrase = this.phrases[phraseIndex];

                if (curPhrase.length === textIndex && numOfDots < 3) {
                    this.setState({
                        textBubble: `${this.state.textBubble} .`,
                        numOfDots: this.state.numOfDots + 1
                    });
                } else if (this.state.numOfDots === 3) {
                    let nextPhraseIndex = phraseIndex + 1;
                    const shouldResetPhrases = numOfDots === 3 &&
                    delayForNextSentence === 3;
                    if (shouldResetPhrases) {
                        this.setState({
                            numOfDots: 0,
                            delayForNextSentence: 0,
                            phraseIndex: shouldResetPhrases && nextPhraseIndex === this.phrases.length ? 0 : nextPhraseIndex,
                            textBubble: "",
                            textIndex: 0,
                        });
                    } else {
                        this.setState({
                            numOfDots: numOfDots === 3 ? numOfDots : numOfDots + 1,
                            delayForNextSentence: numOfDots === 3 && delayForNextSentence < 3 ? delayForNextSentence + 1 : delayForNextSentence,
                        });
                    }
                } else {
                    this.setState({
                        textBubble: `${this.state.textBubble} ${curPhrase[textIndex]}`,
                        textIndex: this.state.textIndex + 1
                    });
                }
            } else {
                this.setState({
                    phraseIndex: 0,
                    numOfDots: 0,
                    delayForNextSentence: 0
                });
            }
            this.setState({
                hasOpenMouth: !this.state.hasOpenMouth
            });
        }, 230);
    }

    receivedImg = (img) => {
        return () => {
            const imgsReceived = this.state.imgsReceived.concat(img);
            this.setState({
                imgsReceived
            });
            if (imgsReceived.length >= 2 && !this.state.startAnimation) {
                this.setState({
                    startAnimation: true
                });
                this.fireAnimation();
            }
        };
    }

    render() {
        return (
            <div className="col-100 centerContainer sasuke_container">
                <div className="my_img_container">
                    <img src="/images/Sasuke.svg" alt="Kiwi standing on oval" onLoad={this.receivedImg("Sasuke")} className={classNames({hide: this.state.hasOpenMouth})} />
                    <img src="/images/Sasuke2.svg" alt="Kiwi standing on oval" onLoad={this.receivedImg("Sasuke2")} className={classNames({hide: !this.state.hasOpenMouth})} />
                    <div className={classNames("talk-bubble-container", {hide: !this.state.startAnimation})}>
                        <div className="talk-bubble">
                            <h3>
                            {this.state.textBubble}
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default connect(mapStateToProps, null)(About_Me);
