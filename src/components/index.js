import React, {Component} from "react";
import LandingPageV2 from "./LandingPageV2";
import AboutMeV2 from "./AboutMeV2";
import MyWorkPlaces from "./WorkExperience";
import MySkills from "./Skills";
import MyProjects from "./ProjectsList";

import ReactGA from "react-ga";

export default class Portfolio extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount = () => {
      ReactGA.pageview(window.location.pathname + window.location.search);
    }

    render = () => {
        return [
          <LandingPageV2 />,
          <AboutMeV2 />,
          <MyWorkPlaces />,
          <MySkills />,
          <MyProjects />
        ];
    }
}
