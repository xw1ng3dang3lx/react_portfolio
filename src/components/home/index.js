import React, {Component} from "react";
import { Link } from "react-router-dom";

/* Analytics */

import { getMemoryPerf, getTimingPerf, getScreenAnalytics } from "../../tools/analytics";
import {connect} from "react-redux";
import {getText} from "../../resources";

/* CSS */
import "./styles.scss";

class HomeView extends Component {
  constructor(props) {
    super(props);
    let myRoles = ["Software Engineer", "Sith Lord", "Full Stack Dev", "DJ/Producer", "ReactJS Enthusiast", "UCSD Alumni"];
    const getRandomIntInclusive = (min, max) => {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    const shuffle = (arr) => {
      for (let i = 0; i < arr.length; i++) {
        const randomIndex = getRandomIntInclusive(0, i+1);
        const tempVal = arr[i];
        arr[i] = arr[randomIndex];
        arr[randomIndex] = tempVal;
      }
      return arr
    }
    const shuffledArr = shuffle(myRoles);
    this.state = {
        myRoles: myRoles, // TODO
        curRoleIndex: 0,
        curRole: "Software Engineer",
        waiting: 6,
        dash: false,
        removing: false,
        adding: false,
        curInterval: null,
    }
  }

  animate = () => {
    if (this.state.waiting > 0 && !this.state.removing && !this.state.adding) {
      let newString = this.state.curRole;
      if (this.state.dash) {
        newString = newString.substring(0, newString.length - 1)
      }
      else {
        newString += "_";
      }
      this.setState({dash: !this.state.dash, curRole: newString, waiting: this.state.waiting-1 });
      if (this.state.waiting - 1 === 0) {
        clearInterval(this.state.curInterval);
        this.setState({removing: true});
        this.animationSetUp();
      }
    }
    else if (this.state.curRole.length > 0 && this.state.removing) {
      let newString = this.state.curRole.substring(0, this.state.curRole.length - 1);
      this.setState({removing: true, curRole: newString, adding: newString.length === 0});
      if (newString.length === 0) {
        clearInterval(this.state.curInterval);
        this.setState({curRoleIndex: (this.state.curRoleIndex+1) % this.state.myRoles.length,removing: false });
        this.animationSetUp();
      }
    }
    else {
      let newString = this.state.myRoles[this.state.curRoleIndex];
      newString = newString.substring(0, this.state.curRole.length+1);
      this.setState({curRole: newString});
      if (newString.length === this.state.myRoles[this.state.curRoleIndex].length) {
        clearInterval(this.state.curInterval);
        this.setState({adding: false, removing: false, waiting: 6, dash: false} );
        this.animationSetUp();
      }
    }

  }
  animationSetUp = () => {
    let curTime;
    if (this.state.removing) {
      curTime = 200;
    }
    else if (this.state.adding) {
      curTime = 150;
    }
    else {
      curTime = 600;
    }
    const curInterval = setInterval(this.animate, curTime);
    this.setState({curInterval: curInterval});
  }
  componentDidMount = () => {
    this.animationSetUp();
    getMemoryPerf();
  }
  componentWillUnmount = () => {
    if (this.state.curInterval) {

      clearInterval(this.state.curInterval);
    }
  }

  render = () => {
      const {nls} = this.props;
      return (
          <section className="content" id="landingPage">
            <div className="col-100">
              <h1 className="">{nls.homeViewIntroduction}</h1>
              <h2 className="myName ">{nls.homeViewMyName}</h2>
              <h3 className="myRole ">{nls.homeViewIam} {this.state.curRole}</h3>
              <button className="transparentTextBtn centered">{nls.homeViewLearnMore}</button>
            </div>
          </section>
      );
  }
}

const mapStateToProps = (state) => {
  return {
    nls: state.langReducer.nls
  }
}

const mapDispatchToProps = (dispatch => {
  return {
  }
});

export default connect(mapStateToProps,mapDispatchToProps)(HomeView);
