import React, {Component} from "react";
import { Link } from "react-router-dom";

import BigTitle from "../common/BigTitle/";
import TalkingSasuke from "../TalkingSasuke";
import ResumeLink from "../ResumeLink";

import classNames from "classnames";
import LazyLoad from "react-lazyload";
import Waypoint from "react-waypoint";

import {connect} from "react-redux";
import Particles from 'react-particles-js';

/* CSS */
import "./styles.scss";

const mapStateToProps = (state) => {
    return {
        windowState: state.windowReducer
    }
}

class LandingPageV2 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            particlesContainer: null
        }
    }

    componentWillMount = () => {
        this.setState({
            particlesContainer: this.getParticlesContainer()
        });
    }

    componentWillUpdate = (nextProps) => {
        if (this.props.windowState.screenWidth !== nextProps.windowState.screenWidth ||
            this.props.windowState.screenHeight !== nextProps.windowState.screenHeight) {
            const nextScreenSize = {
                screenWidth: nextProps.windowState.screenWidth,
                screenHeight: nextProps.windowState.screenHeight
            };
            this.setState({
                particlesContainer: this.getParticlesContainer(nextScreenSize)
            });
        }
    }

    getParticlesContainer = (props = {}) => {
        const {screenWidth: curScreenWidth, screenHeight: curScreenHeight} = this.props.windowState,
            width = props.screenWidth ? props.screenWidth : curScreenWidth,
            height = props.screenHeight ? props.screenHeight : curScreenHeight;

        const ParticlesContainerStyles = {
            position: "absolute",
            width: `${width}px`,
            height: `${height}px`
        },
        ParticlesStyles = {
            position: "relative",
            width: `${width}px`,
            height: `${height}px`
        };
        return (
            <div className="particlesContainer" style={ParticlesContainerStyles}>
                <Particles style={ParticlesStyles}/>
            </div>
        );
    }

    render = () => {
        const ParticlesContainer = this.state.particlesContainer;

        return (
            <div className="col-100 centerContainer about_me_section">
                {ParticlesContainer &&
                     ParticlesContainer
                }
                <div className="contentContainer col-80 centeredCol">
                    <BigTitle
                        center={true}
                        title="Anthony Wang"
                    />
                    <TalkingSasuke />
                    <div className="scroll_to_next_section">
                        <a href="/resume.pdf" className="transparentTextBtn whiteText centered">Learn More</a>
                    </div>
                </div>
            </div>
        );
    }
}
export default connect(mapStateToProps, null)(LandingPageV2);
