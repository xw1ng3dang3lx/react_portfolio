import React, {Component} from "react";
import LazyLoad from "react-lazyload";

/* CSS */
import "./styles.scss";

export default class LargeImageWithCaption extends Component {
    constructor(props) {
        super(props);
    }

    getStyles = () => {
      const {maxWidth} = this.props;
      if (!maxWidth) {
        return {}
      }
      return {
        maxWidth: maxWidth
      };
    }

    render() {
        const {imgSrc, caption, maxWidth} = this.props;
        return (
            <div className="large-image-container" style={this.getStyles()}>
                <LazyLoad height={200} offset={200}>
                    <img src={imgSrc} />
                </LazyLoad>
                {caption &&
                    <h3>{caption}</h3>
                }
            </div>
        );
    }
};
