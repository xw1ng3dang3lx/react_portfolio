import React, {Component} from "react";
import LazyLoad from "react-lazyload";
import Divider from "../Divider";
/* CSS */
import "./styles.scss";

export default class ImageCard extends Component {
    constructor(props) {
        super(props);
    }

    render = () => {
        return (
            <div className="img-card-container">
                <div className="img-card-header">
                    <LazyLoad height={200} offset={200}>
                        <img src={this.props.img}></img>
                    </LazyLoad>
                </div>
                <div className="img-card-title">
                    <h1>{this.props.title}</h1>
                </div>
                <div className="img-card-label">
                    <h2>{this.props.label}</h2>
                </div>
                <Divider />
            </div>
        );
    }
}
