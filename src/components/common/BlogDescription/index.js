import React from "react";

/* CSS */
import "./styles.scss";

export const BlogDescription = ({description}) => {
  return (
    <p className={"blog-description"}>
      {description}
    </p>
  );
}

export default BlogDescription;
