import React, {Component} from "react";
import classNames from "classnames";

/* CSS */
import "./styles.scss";

export default class MediumTitle extends Component {
  constructor(props) {
    super(props);
  }

  render = () => {
    const centerText = classNames({
      "center": this.props.center || false,
    });
    return (
      <div className="header-container">
        <h2 className={centerText}>{this.props.title}</h2>
      </div>
    );
  }
}
