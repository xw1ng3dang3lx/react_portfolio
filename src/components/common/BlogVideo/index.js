import React, {PureComponent} from "react";
import ReactPlayer from "react-player";
import ReactGA from "react-ga";

import "./styles.scss";

class BlogVideo extends PureComponent {
  constructor(props) {
    super(props);
  }

  handleEnd = () => {
    const {url} = this.props;
    ReactGA.event({
      category: "movie_player",
      action: "ended",
      label: url
    });
  }

  handleStart = () => {
    const {url} = this.props;
    ReactGA.event({
      category: "movie_player",
      action: "started",
      label: url,
      value: this.player.getCurrentTime()
    });
  }

  handlePause = () => {
    const {url} = this.props;
    ReactGA.event({
      category: "movie_player",
      action: "pause",
      label: url,
      value: this.player.getCurrentTime()
    });
  }

  ref = player => {
    this.player = player
  }

  render = () => {
    return (
      <div className="col-100 centered-blog-video">
        <ReactPlayer
          className="react-player"
          {...this.props}
          controls
          ref={this.ref}
          onEnded={this.handleEnd}
          onStart={this.handleStart}
          onPause={this.handlePause}
        />
      </div>
    );

  }
}

export default BlogVideo;
