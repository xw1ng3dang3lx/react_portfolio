import React, {PureComponent} from "react";
import BlogHeader from "../BlogHeader";
import BlogDescription from "../BlogDescription";
import classNames from "classnames";

import {connect} from "react-redux";

/* CSS */
import "./styles.scss";

export class CenteredList extends PureComponent {
    constructor(props) {
        super(props);
    }

    render = () => {
        const {nls, title, listOfItems} = this.props;
        return (
            <div className="center-list-container">
                <div className="center-list-text">
                    {title &&
                        <BlogHeader title={nls[title]} />
                    }
                </div>
                <ul>
                  {listOfItems.map(key => nls[key]).map((item, idx) => {
                      return <li key={idx}>{item}</li>
                  })}
                </ul>
            </div>
        );
    }
};

const mapStateToProps = (state) => {
    return {
        nls: state.langReducer.nls,
    }
};

export default connect(mapStateToProps, null)(CenteredList);
