import React from "react";

/* CSS */
import "./styles.scss";

export const BlogHeader = ({title}) => {
  return (
    <h1 className={"blog-header"}>
      {title}
    </h1>
  );
}

export default BlogHeader;
