import React, {Component} from "react";
import BlogHeader from "../BlogHeader";
import BlogDescription from "../BlogDescription";
import classNames from "classnames";

/* CSS */
import "./styles.scss";

export default class CenteredBlogText extends Component {
    constructor(props) {
        super(props);
    }

    render = () => {
        const {includeDivider, title, description} = this.props;
        return (
            <div className="center-blog-container">
                <div className="center-blog-text">
                    {title &&
                        <BlogHeader title={title} />
                    }
                    <BlogDescription description={description} />
                </div>
                {includeDivider &&
                    <div className="divider"/>
                }
            </div>
        );
    }
};
