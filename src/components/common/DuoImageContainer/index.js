import React, {Component} from "react";
import LazyLoad from "react-lazyload";
import Divider from "../Divider";

/* CSS */
import "./styles.scss";

export default class DuoImageContainer extends Component {
    constructor(props) {
        super(props);
    }

    render = () => {
        return (
            <div className="duo-img-card-container">
                <div className="img-container">
                    <LazyLoad height={320} offset={200}>
                        <img className="img-card-1" src={this.props.img_1}></img>
                    </LazyLoad>
                    <LazyLoad height={320} offset={200}>
                        <img className="img-card-2" src={this.props.img_2}></img>
                    </LazyLoad>
                </div>
                <div className="img-label">
                    <h2>{this.props.label}</h2>
                </div>
                <Divider />
            </div>
        );
    }
}
