import React, {Component} from "react";
import classNames from "classnames";

/* CSS */
import "./styles.scss";

export default class BigTitle extends Component {
  constructor(props) {
    super(props);
  }

  render = () => {
    const centerText = classNames({
      "center": this.props.center || false,
    });
    return (
      <div className="header-container">
        <h1 className={centerText}>{this.props.title}</h1>
      </div>
    );
  }
}
