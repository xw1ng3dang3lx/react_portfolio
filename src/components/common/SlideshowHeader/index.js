import React, {Component} from "react";
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import "./styles.scss";

class SlideshowHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
          idx: 0,
          transitionTime: 7500,
        };

        const {thumbnails} = this.props;

        if (thumbnails.length > 1) {
          this.startSlideShow();
        }
    }

    startSlideShow = () => {
      const {thumbnails} = this.props;
      setInterval(() => {
        const {idx} = this.state,
          nextIdx = (idx + 1) % thumbnails.length;
        this.setState({
          idx: nextIdx
        });
      }, 7500);
    }

    render = () => {
        const {thumbnails} = this.props,
          {idx, transitionTime} = this.state,
          thumbnail = thumbnails[idx];

        const backgroundStyle = {
          "backgroundImage" : "url(" + thumbnail + ")"
        };

        return (
          <div className="projectHeader col-100">
              <ReactCSSTransitionGroup
                  transitionName="thing"
                  transitionEnterTimeout={transitionTime/2}
                  transitionLeaveTimeout={transitionTime/2}>
                <img src={thumbnail}  key={idx} />
              </ReactCSSTransitionGroup>
              <div className="opacityOverlay" />
              <div className="opaqueOverlay" />
          </div>
        );

        /*
        return (
            <div className="projectHeaderContainer">
              <ReactCSSTransitionGroup
                  transitionName="thing"
                  transitionEnterTimeout={1000}
                  transitionLeaveTimeout={500}>
                <div className="projectHeader col-100" key={idx} style={backgroundStyle}>
                    <div className="opacityOverlay" />
                    <div className="opaqueOverlay" />
                </div>
              </ReactCSSTransitionGroup>
            </div>
        );
        */
    }
}

export default SlideshowHeader;
