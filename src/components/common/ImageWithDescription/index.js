import React, {Component} from "react";
import BlogHeader from "../BlogHeader";
import BlogDescription from "../BlogDescription";

/* CSS */
import "./styles.scss";

export default class ImageWithDescription extends Component {
    constructor(props) {
        super(props);
    }

    getImgContainer = () => {
        const {imgSrc} = this.props;
        return (
            <div className="image-container col-50">
                <img src={imgSrc} />
            </div>
        );
    }

    getDescriptionContainer = () => {
        const {title, description} = this.props;
        return (
            <div className="blog-container col-50">
                {title &&
                  <BlogHeader title={title} />
                }
                {
                    description.map(text => <BlogDescription description={text} />)
                }
            </div>
        );
    }
    render() {
        const {title, description, imgSrc, includeDivider, imgOnLeft} = this.props;
        let LeftContainer = this.getDescriptionContainer(),
            RightContainer = this.getImgContainer();
        if (imgOnLeft) {
            const tmpContainer = LeftContainer;
            LeftContainer = RightContainer;
            RightContainer = tmpContainer;
        }
        return (
            <div className="image-with-description-container">
                {LeftContainer}
                {RightContainer}
                {includeDivider &&
                    <div className="divider"/>
                }
            </div>
        );
    }
};
