import React, {PureComponent} from "react";

import {connect} from "react-redux";

import "./styles.scss";

class Footer extends PureComponent {
  constructor(props) {
    super(props);
  }

  routeTo = (route) => {
    return () => {
      ReactGA.event({
        category: "footer",
        action: "routedTo",
        label: route
      });

    };
  }

  render() {
    const {nls} = this.props;
    return (
        <footer className="footer">
            <h1>{nls.footerLetsGetInTouch}</h1>
            <ul className="links">
                <li><a onClick={this.routeTo("mail")} href="mailto:anthony.wang@me.com"><i className="zmdi zmdi-email"></i></a></li>
                <li><a onClick={this.routeTo("linkedin")} href="https://www.linkedin.com/in/anthony-wang-19193692"><i className="zmdi zmdi-linkedin"></i></a></li>
                <li><a onClick={this.routeTo("soundcloud")} href="https://soundcloud.com/saskrillex"><i className="zmdi zmdi-soundcloud"></i></a></li>
            </ul>
            <div className="copyright">
                <span>{nls.footerCopyRight}</span>
            </div>
        </footer>
    );
  }
}

const mapStateToProps = (state) => {
    return {
        nls: state.langReducer.nls
    };
}

export default connect(mapStateToProps, null)(Footer);
