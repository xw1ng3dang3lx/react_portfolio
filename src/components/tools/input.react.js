import React, {Component} from "react";
import PropTypes from "prop-types";

class Input extends Component {
  constructor(props) {
    super(props);
    this.state = { searchInput: this.props.value} ;
  }

  handleChange = (event) => {
    this.setState({ searchInput: event.target.value });
    this.props.handleChange(event.target.value)
  }

  render = () => {
    return (
      <input
        type={this.props.type}
        placeholder={this.props.placeholder || ""}
        value={this.state[this.props.name]}
        onChange={this.handleChange}>
      </input>
    );
  }
}

Input.PropTypes = {
  name: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  value: PropTypes.string
}

export default Input;
