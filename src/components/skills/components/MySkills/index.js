import React, {Component} from "react";
import CircleHeader from "../CircleHeader";

import {mySkills} from "../../config";

import {connect} from "react-redux";

export class MySkills extends Component {
    constructor(props) {
        super(props);
    }

    render = () => {
        const {nls} = this.props;
        return (
            <div className="material-card-container">
                {
                    mySkills.map((skill, index) => {
                        return (
                            <div className="col-33" key={index}>
                                <CircleHeader
                                    accentColour={skill.color}
                                    title={nls[skill.title]}
                                    icon={skill.icon}
                                    list={skill.skills.map(skill => nls[skill])}
                                />
                            </div>
                        );
                    })
                }
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        nls: state.langReducer.nls
    };
}

export default connect(mapStateToProps, null)(MySkills);
