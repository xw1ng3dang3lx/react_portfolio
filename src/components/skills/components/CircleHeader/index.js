import React, {Component} from "react";
import BigTitle from "../../../common/BigTitle/";
import classNames from "classnames";

import "./styles.scss";

export default class CircleHeader extends Component {
    constructor(props) {
        super(props);
    }

    render = () => {
        const customStyle = {
            backgroundColor: this.props.accentColour ?
                this.props.accentColour : null
        };

        return (
            <div className="circle-header-container">
                <div className="circle-header" style={customStyle}>
                    <i className={classNames("zmdi", this.props.icon)}></i>
                </div>
                <div className="circle-title">
                    <h1>{this.props.title}</h1>
                    <div className="divider" style={customStyle}></div>
                </div>
                <div className="circle-skills-container">
                    <ul>
                        {
                            (this.props.list).map((item, index) => {
                                return <li key={index}>{item}</li>
                            })
                        }
                    </ul>
                </div>
            </div>
        );
    }
}
