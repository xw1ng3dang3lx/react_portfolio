import React, {Component} from "react";

import BigTitle from "../common/BigTitle/";
import MySkills from "./components/MySkills";

import { connect } from "react-redux"

export const SkillsSection = ({nls}) => {
  return (
    <div className="col-100 centerContainer">
      <div className="col-80 centeredCol">
          <BigTitle
              center={true}
              title={nls.skillsTitle}
          />
          <MySkills />
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
    return {
        nls: state.langReducer.nls
    };
}

export default connect(mapStateToProps, null)(SkillsSection);
