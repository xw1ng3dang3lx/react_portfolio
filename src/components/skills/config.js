export const
    mySkills = [
        {
            title: "skillsFrontEnd",
            color: "#2196F3",
            skills: ["skillsReact", "skillsFlux", "skillsES6", "skillsWebpack", "skillsCSS", "skillsAngular1", "skillsAngular2", "skillsHTML"],
            icon: "zmdi-desktop-mac"
        },
        {
            title: "skillsBackEnd",
            color: "#9C27B0",
            skills: ["skillsNode", "skillsGo", "skillsSQL"],
            icon: "zmdi-storage"
        },
        {
            title: "skillsOther",
            color: "#009688",
            skills: ["skillsDeployment", "skillsFireBase", "skillsStaticWeb", "skillsIOSDev"],
            icon: "zmdi-desktop-mac"
        }
    ];
