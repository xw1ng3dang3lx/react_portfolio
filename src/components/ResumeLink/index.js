import React, {Component} from "react";
import { Link } from "react-router-dom";

import BigTitle from "../common/BigTitle/";
import TalkingSasuke from "../TalkingSasuke";

import classNames from "classnames";
import LazyLoad from "react-lazyload";
import Waypoint from "react-waypoint";

import {connect} from "react-redux";

import ReactGA from "react-ga";

/* CSS */
import "./styles.scss";

/* Analytics */
export const ResumeLink = ({nls}) => {
  const handleVisitClick = () => {
    ReactGA.event({
      category: "ResumeLink",
      action: "visited"
    });
  }

  return (
    <div className="resume_download_ctr">
        <a onClick={handleVisitClick} href="/resume.pdf" className="transparentTextBtn blackText centered">
          {nls.resume}
        </a>
    </div>
  )
};

const mapStateToProps = (state) => {
    return {
        nls: state.langReducer.nls
    };
}

export default connect(mapStateToProps, null)(ResumeLink);
