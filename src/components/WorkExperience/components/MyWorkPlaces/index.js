import React, {Component} from "react";
import ImgCard from "../../../common/ImageCard";

import {myWorkPlaces} from "../../config";

import {connect} from "react-redux";

export class MyWorkPlaces extends Component {
    constructor(props) {
        super(props);
    }

    render = () => {
        const {nls} = this.props;
        return (
            <div className="material-card-container">
                {
                    myWorkPlaces.map( (workplace, index) => {
                        return (
                            <div className="col-33" key={index}>
                                <ImgCard
                                    title={nls[workplace.title]}
                                    label={nls[workplace.label]}
                                    img={workplace.img}
                                />
                            </div>
                        );
                    })
                }
            </div>
        );
    }
};

const mapStateToProps = (state) => {
    return {
        nls: state.langReducer.nls
    };
}

export default connect(mapStateToProps, null)(MyWorkPlaces);
