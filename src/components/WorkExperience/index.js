import React, {Component} from "react";

import BigTitle from "../common/BigTitle/";
import MyWorkPlaces from "./components/MyWorkPlaces";

import { connect } from "react-redux"

export const WorkExperience = ({nls}) => {
    return (
        <div className="col-100 centerContainer">
            <BigTitle
                center={true}
                title={nls.workExpTitle}
            />
            <div className="col-80 centeredCol">
                <MyWorkPlaces />
            </div>
        </div>
    );
}

const mapStateToProps = (state) => {
    return {
        nls: state.langReducer.nls
    };
}

export default connect(mapStateToProps, null)(WorkExperience);
