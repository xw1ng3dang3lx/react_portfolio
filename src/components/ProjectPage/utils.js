import QBOA_Blog from "./components/QBOA_Blog";
import Bookmarx_Blog from "./components/Bookmarx_Blog";
import Tinder_Blog from "./components/Tinder_Blog";
import Portfolio_Blog from "./components/Portfolio_Blog";
import ArduinoBlog from "./components/ArduinoBlog";

export const
    getBlog = (pid) => {
        let Blog = null;
        switch (parseInt(pid)) {
            case 0:
                Blog = QBOA_Blog;
                break;
            case 1:
                Blog = Tinder_Blog;
                break;
            case 3:
                Blog = Bookmarx_Blog;
                break;
            case 4:
                Blog = ArduinoBlog;
                break;
            default:
        }
        return Blog;
    };
