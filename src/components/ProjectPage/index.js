import React, {Component} from "react";
import ProjectTagsContainer from "./components/ProjectTagsContainer";
import Header from "./components/Header";
import {connect} from "react-redux";

import {getBlog} from "./utils";
import {projects} from "./configs";

import ReactGA from "react-ga";

/* CSS */
import "./styles.scss";

class ProjectPage extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount = () => {
        window.scrollTo(0, 0);
        ReactGA.pageview(window.location.pathname + window.location.search);
    }

    getProjectID = () => {
      const {match} = this.props,
        {pid} = match.params;
      return pid;
    }

    render = () => {
        const pid = this.getProjectID();

        if (pid === undefined || pid >= projects.length || !projects[pid]) {
          return nil
        }

        const projectList =
          projects.slice().sort((a, b) => parseInt(a.pid) - parseInt(b.pid))
        const Blog = getBlog(pid),
          project = projectList[pid];

        return (
            <div className="project-page">
                <Header
                    windowState={this.props.windowState}
                    {...project}
                />
                <ProjectTagsContainer project={project}/>
                {Blog &&
                    <Blog />
                }
            </div>

        );

    }
}

const mapStateToProps = (state) => {
    return {
        windowState: state.windowReducer
    }
};

export default connect(mapStateToProps, null)(ProjectPage);
