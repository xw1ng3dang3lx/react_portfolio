import React, {Component} from "react";
import BigTitle from "../../../common/BigTitle";
import SlideshowHeader from "../../../common/SlideshowHeader";
import {connect} from "react-redux";

import "./styles.scss";

class ProjectHeader extends Component {
    constructor(props) {
        super(props);
    }

    render = () => {
        const {nls, title, headerImgs} = this.props;
        return [
            <SlideshowHeader thumbnails={headerImgs}/>,
            <BigTitle title={nls[title]}/>
        ];

    }
}

const mapStateToProps = (state) => {
    return {
        nls: state.langReducer.nls,
    }
};

export default connect(mapStateToProps, null)(ProjectHeader);
