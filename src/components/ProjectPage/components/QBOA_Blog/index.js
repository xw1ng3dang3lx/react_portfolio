import React, {PureComponent} from "react";
import CenteredBlogText from "../../../common/CenteredBlogText";
import ImageWithDescription from "../../../common/ImageWithDescription";
import LargeImgContainer from "../../../common/LargeImgContainer";
import DuoImageContainer from "../../../common/DuoImageContainer";

import {connect} from "react-redux";

class QBOA_Blog extends PureComponent {
    constructor(props) {
        super(props);
    }

    render = () => {
        const {nls} = this.props;

        return (
            <div className="col-100">
                <ImageWithDescription
                    title={nls.qboaHeader1}
                    description={[nls.qboaParagraph1, nls.qboaParagraph2]}
                    imgSrc="/images/qboa-3.png"
                    includeDivider={true}
                    imgOnLeft={true}
                />
                <CenteredBlogText
                    title={nls.qboaHeader2}
                    description={nls.qboaParagraph3}
                />
                <LargeImgContainer
                    imgSrc="/images/qboa-0.png"
                />
                <DuoImageContainer
                    img_1={"/images/qboa-1.png"}
                    img_2={"/images/qboa-2.png"}
                    label={nls.qboaImgCaption1}
                />
            </div>
        );

    }
}

const mapStateToProps = (state) => {
    return {
        nls: state.langReducer.nls,
    }
};

export default connect(mapStateToProps, null)(QBOA_Blog);
