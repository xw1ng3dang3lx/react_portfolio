import React, {Component} from "react";

import {connect} from "react-redux";
import ProjectTag from "../ProjectTags";

import "./styles.scss";

class ProjectTagsContainer extends Component {
    constructor(props) {
        super(props);
    }

    getProjectTags = () => {
      const {nls, project} = this.props,
          projectTags = [
              {
                  title: nls.typeText,
                  tags: project.type.map(id => nls[id]),
                  link: ""
              },
              {
                  title: nls.roleText,
                  tags: project.role.map(id => nls[id]),
                  link: ""
              },
              {
                  title: nls.technologiesText,
                  tags: project.tags.map(id => nls[id]),
                  link: ""
              }
          ];

      if (project.githubLink) {
          projectTags.push({
            title: nls.typeCode,
            tags: [nls.codeLink],
            link: project.githubLink
          })
      }

      return projectTags;
    }

    render() {
        const {nls} = this.props,
            projectTags = this.getProjectTags();

        return (
            <div className="projectContentCtr col-100">
                <div className="projectContent">
                    <div className="projectTagsCtr">
                    {
                        projectTags
                          .filter(project => project.tags.length > 0)
                          .map((project, index) => {
                              return (
                                  <ProjectTag
                                      key={index}
                                      tagInfo={project}
                                  />);
                              })
                    }
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        nls: state.langReducer.nls,
    }
};

export default connect(mapStateToProps, null)(ProjectTagsContainer);
