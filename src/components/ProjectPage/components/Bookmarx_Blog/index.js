import React, {Component} from "react";
import CenteredBlogText from "../../../common/CenteredBlogText";
import ImageWithDescription from "../../../common/ImageWithDescription";
import LargeImgContainer from "../../../common/LargeImgContainer";

class Bookmarx_Blog extends Component {
    constructor(props) {
        super(props);
    }

    render = () => {

        return (
            <div className="col-100">
                <ImageWithDescription
                    title={"The Problems"}
                    description={["meow"]}
                    imgSrc="/images/placeholder.jpg"
                    includeDivider={true}
                    imgOnLeft={true}
                />
                <ImageWithDescription
                    title={"The Problems"}
                    description={["meow"]}
                    imgSrc="/images/placeholder.jpg"
                    includeDivider={true}
                    imgOnLeft={false}
                />
                <CenteredBlogText
                    title={"Some More Problems"}
                    description="meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow"
                    includeDivider={false}
                />
                <CenteredBlogText
                    description="meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow"
                    includeDivider={true}
                />
            </div>
        );

    }
}

export default Bookmarx_Blog;
