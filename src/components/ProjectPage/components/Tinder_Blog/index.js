import React, {Component} from "react";
import CenteredBlogText from "../../../common/CenteredBlogText";
import ImageWithDescription from "../../../common/ImageWithDescription";
import LargeImgContainer from "../../../common/LargeImgContainer";

import {connect} from "react-redux";

export class Tinder_Blog extends Component {
    constructor(props) {
        super(props);
    }

    render = () => {
        const {nls} = this.props;
        return (
            <div className="col-100">
                <ImageWithDescription
                    title={nls.theGoal}
                    description={[nls.tinderTheProblemDescription]}
                    imgSrc="/images/tinder-1.png"
                    includeDivider={true}
                    imgOnLeft={true}
                />
            </div>
        );

    }
}

const mapStateToProps = (state) => {
    return {
        nls: state.langReducer.nls,
    }
};

export default connect(mapStateToProps, null)(Tinder_Blog);
