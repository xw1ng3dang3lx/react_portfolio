import React, {Component} from "react";
import CenteredBlogText from "../../../common/CenteredBlogText";
import ImageWithDescription from "../../../common/ImageWithDescription";
import LargeImgContainer from "../../../common/LargeImgContainer";
import CenteredList from "../../../common/CenteredList";
import BlogVideo from "../../../common/BlogVideo";
import LazyLoad from "react-lazyload";
import {connect} from "react-redux";

import "./styles.scss";

class ArduinoBlog extends Component {
    constructor(props) {
        super(props);
    }

    render = () => {

        return (
            <div className="col-100">
                <LazyLoad height={360} offset={200}>
                    <BlogVideo url="/movies/midi_controller.mov" />
                </LazyLoad>
                <CenteredList
                    title="supplies"
                    listOfItems={["midiControllerSuppliesUno", "midiControllerSuppliesRegisters", "midiControllerSuppliesButtons", "midiControllerSuppliesResistors"]}
                />
                <CenteredBlogText
                    title={"Some More Problems"}
                    description="meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow"
                    includeDivider={false}
                />
                <LargeImgContainer
                    imgSrc="/images/arduino_schemaic.png"
                    caption="caption"
                    maxWidth={780}
                />
                <CenteredBlogText
                    description="meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow meow"
                    includeDivider={true}
                />
            </div>
        );

    }
}

const mapStateToProps = (state) => {
    return {
        nls: state.langReducer.nls,
    }
};

export default connect(mapStateToProps, null)(ArduinoBlog);
