import React, {Component} from "react";

import ReactGA from "react-ga";

import "./styles.scss";

export class ProjectTag extends Component {
  constructor(props) {
    super(props);
  }

  getTagList = () => {
    const tag = this.props.tagInfo,
        tagList = (tag.tags || []).map((tagTitle, index) => {
          if (tag.link) {
            const handleVisitClick = () => {
              ReactGA.event({
                category: "GithubLink",
                action: "visited",
                label: tag.link
              });
              window.location.href = tag.link;
            };

            return (<li className="tag-link" key={index} onClick={handleVisitClick}>
                {tagTitle}
              </li>
            );
          }

          return (<li key={index}>
              {tagTitle}
            </li>
          );
        });
    return tagList;
  }

  render = () => {
    const tag = this.props.tagInfo;

    return (
      <div className="projectTags">
          <h4>{tag.title}</h4>
          <ul>
            {this.getTagList()}
          </ul>
      </div>
    );
  }
}

export default ProjectTag;
