import React, {Component} from "react";
import Cloud from "./components/Cloud";
import Sun from "./components/Sun";
import Grass from "./components/Grass";
import SakuraTree from "./components/SakuraTree";
import Water from "./components/Water";
import Mountain from "./components/Mountain";

import {initialScrollPercentage} from "./configs";

import {connect} from "react-redux";

/* CSS */
import "./styles.scss";

class MaterialMountain extends Component {
    constructor(props) {
        super(props);

        this.state = {
            curTimeOfDay: "day",
            numOfGrass: 0,
            scrollPercentage: initialScrollPercentage
        };

    }

    componentDidMount = () => {
        if (this.ref) {
            this.setState({
                numOfGrass: parseInt(this.ref.offsetWidth / 45),
                canvasHeight: this.ref.offsetHeight,
                canvasWidth: this.ref.offsetWidth
            });
            this.mousemove = document.addEventListener("mousewheel", this.mouseMove, false);
            this.mousemoveFF = document.addEventListener("DOMMouseScroll", this.mouseMove, false);
        }
    }

    componentWillUnmount = () => {
        document.removeEventListener("mousewheel", this.mousemove);
        document.removeEventListener("DOMMouseScroll", this.mousemoveFF);
    }

    handleRef = (ref) => {
        this.ref = ref;
    }

    getGrassContainer = () => {
        const grassContainer = [];
        for (let i = 0; i < this.state.numOfGrass; i++) {
            grassContainer.push(<Grass curTimeOfDay={this.state.curTimeOfDay} />);
        }
        return grassContainer;
    }

    mouseMove = () => {
        if (document.body.scrollTop < this.state.canvasHeight) {
            const scrollPercentage = ((document.body.scrollTop / this.state.canvasHeight) * 100) + initialScrollPercentage;
            this.setState({scrollPercentage: scrollPercentage});

            this.setTimeOfDay(scrollPercentage);
        }
    }

    setTimeOfDay = (scrollPercentage) => {
        let nextTimeOfDay = this.state.curTimeOfDay;
        const mod = (scrollPercentage % 100);

        if ((mod >= 80 && mod <= 100)) {
            nextTimeOfDay = "sunset";
        } else if (mod >= 60 && mod < 80) {
            nextTimeOfDay = "day";
        } else {
            nextTimeOfDay = "night";
        }

        this.setState({
            curTimeOfDay: nextTimeOfDay
        });

    }

    render = () => {
        const {nls} = this.props,
            grassContainer = this.getGrassContainer();

        return (
            <section className={`content ${this.state.curTimeOfDay}`} id="landingPage" ref={this.handleRef} style={{padding: 0}}>
                <Cloud className="cloud-1"/>
                <Cloud className="cloud-2"/>
                <Cloud className="cloud-3"/>
                <Mountain
                    canvasHeight={this.state.canvasHeight}
                    canvasWidth={this.state.canvasWidth}
                    curTimeOfDay={this.state.curTimeOfDay}
                />
                <Sun
                    canvasHeight={this.state.canvasHeight}
                    canvasWidth={this.state.canvasWidth}
                    curTimeOfDay={this.state.curTimeOfDay}
                    scrollPercentage={this.state.scrollPercentage}
                />

                <div className="grassContainer">
                    {grassContainer}
                </div>
                <div className="sakura-tree">
                    <img src="/images/springTree.svg" alt="Kiwi standing on oval" />
                </div>
                <Water curTimeOfDay={this.state.curTimeOfDay} />

            </section>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        nls: state.langReducer.nls
    }
}

const mapDispatchToProps = (dispatch => {
    return {
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(MaterialMountain);
