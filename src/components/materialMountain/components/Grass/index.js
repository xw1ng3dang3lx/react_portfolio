import React, {Component} from "react";

/* CSS */
import "./styles.scss";

class Grass extends Component {
    constructor(props) {
        super(props);
    }

    render = () => {
        const {style} = this.props;

        return (
            <div className="grass" style={style}>
                <div className="grass-blade-1"></div>
                <div className="grass-blade-2"></div>
                <div className="grass-blade-3"></div>
                <div className="grass-blade-4"></div>
            </div>
        );
    }
}

Grass.defaultProps = {
    style: {}
};

export default Grass;
