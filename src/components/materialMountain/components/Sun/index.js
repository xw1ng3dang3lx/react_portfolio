import React, {Component} from "react";

import {initialScrollPercentage} from "../../configs";

/* CSS */
import "./styles.scss";

class Sun extends Component {
    constructor(props) {
        super(props);

        this.state = {
            y: 0,
            transform: ""
        }
    }

    componentDidMount = () => {
        if (this.ref) {
            this.setState({
                sunHeight: this.ref.offsetHeight,
                sunWidth: this.ref.offsetWidth
            });
        }
    }

    componentWillUpdate = (nextProps) => {
        if (!this.props.canvasHeight && nextProps.canvasHeight) {
            const
                radius = (Math.max(nextProps.canvasHeight, nextProps.canvasWidth) / 2),
                radian = ((360 * (initialScrollPercentage / 100)) * Math.PI) / 180;
            const posY = Math.sin( radian ) * radius + radius;
            this.setState({
                y: posY
            });

        }
        if (this.props.scrollPercentage !== nextProps.scrollPercentage) {
            const transform = this.animateSunMovement(nextProps);
            this.setState({
                transform: transform
            });
        }
    }

    handleRef = (ref) => {
        this.ref = ref;
    }

    rotate = (nextProps) => {
        const {canvasWidth, canvasHeight, scrollPercentage} = nextProps,
            radian = ((360 * (scrollPercentage / 100)) * Math.PI) / 180,
            radius = (Math.max(canvasHeight, canvasWidth) / 2),
            posX = Math.cos( radian ) * radius,
            posY = Math.sin( radian ) * radius + radius;


        if (!isNaN(posX) && !isNaN(posY)) {
            const translate = `translate(${posX}px, ${posY}px)`;

            return translate;

        } else {
            return "";
        }
    }

    animateSunMovement = (nextProps) => {
        return this.rotate(nextProps);
    }

    render = () => {
        const {curTimeOfDay} = this.props;
        return (
            <div
                className={`sun ${curTimeOfDay}`}
                ref={this.handleRef}
                style={{
                    top: this.state.y,
                    transform: this.state.transform
                }}
            />
        );
    }
}

export default Sun;
