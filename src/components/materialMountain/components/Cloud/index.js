import React, {Component} from "react";

/* CSS */
import "./styles.scss";

class Cloud extends Component {
    constructor(props) {
        super(props);
    }

    render = () => {

        return (
            <div className={`cloud ${this.props.className}`}>
                <div className="bubble"></div>
                <div className="bubble"></div>
                <div className="bubble"></div>
                <div className="bubble"></div>
                <div className="base"></div>
            </div>
        );
    }
}

Cloud.defaultProps = {
    className: ""
};

export default Cloud;
