import React, {Component} from "react";

/* CSS */
import "./styles.scss";

class SakuraTree extends Component {
    constructor(props) {
        super(props);
    }

    render = () => {
        return (
            <div className="sakura-tree-container">
                <div className="sakura-bush bush-1">
                    <div className="inner-circle" />
                </div>
                <div className="sakura-bush bush-2">
                    <div className="inner-circle" />
                </div>
                <div className="sakura-bush bush-3">
                    <div className="inner-circle" />
                </div>
            </div>
        );
    }
}

export default SakuraTree;
