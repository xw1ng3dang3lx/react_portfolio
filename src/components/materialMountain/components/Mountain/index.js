import React, {Component} from "react";

/* CSS */
import "./styles.scss";

class Mountain extends Component {
    constructor(props) {
        super(props);
    }

    render = () => {
        return (
            <div className="mountain-container">
                <div className="mountain">
                    <div className="summit">
                        <div className="summit-cap-1"></div>
                        <div className="summit-cap-2"></div>
                        <div className="summit-cap-3"></div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Mountain;
