import React, {Component} from "react";
import classNames from "classnames";
import { withRouter } from "react-router-dom";

import RouterButton from "./components/RouterButton";
import NavButtonWithIcon from "./components/NavButtonWithIcon";
import {connect} from "react-redux";

import ReactGA from "react-ga";

import "./styles.scss";

class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false
    };
  }

  handleNavBarToggle = () => {
    const curState = this.state.isOpen;
    this.setState({
      isOpen: !curState
    });
    ReactGA.event({
      category: "navbar",
      action: "toggledNavbar",
      label: "toggleTo:" + !curState
    });
  }

  handleOutsideClick = () => {
    this.setState({
      isOpen: false
    });
  }

  routeTo = (route) => {
    return () => {
      const {history} = this.props;
      history.push(route);
    };
  }

  render() {
    const {history, nls} = this.props;

    return (
      <div>
        <div className={classNames("nav-container", {isExpanded: this.state.isOpen})} onClick={this.handleOutsideClick} />
        <nav className="fixed-nav">
            <ul className="nav-header">
                <NavButtonWithIcon
                  routeLocation={"/"}
  								icon={"zmdi-menu"}
  								classes={"nav-hamburger"}
                  onClick={this.handleNavBarToggle}
                />
                <RouterButton
                  routeLocation={"/"}
                  label={nls.navBarMe}
  								classes={"me-header"}
                />
            </ul>
            <ul className={classNames("centered-nav", {isExpanded: this.state.isOpen})}>
                <RouterButton
                  routeLocation={"/skills"}
                  label={nls.navBarSkills}
                />
                <RouterButton
                  routeLocation={"/projects"}
                  label={nls.navBarProjects}
                />
                <RouterButton
                  routeLocation={"/timeline"}
                  label={nls.navBarTimeline}
                />
            </ul>
        </nav>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
    return {
        nls: state.langReducer.nls
    };
}

export default connect(mapStateToProps, null)(withRouter(Navbar));
