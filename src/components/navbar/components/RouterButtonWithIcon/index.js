import React, {PureComponent} from "react";
import { withRouter } from "react-router-dom";
import classNames from "classnames";
import ReactGA from "react-ga";

import "./styles.scss";

class RouterButtonWithIcon extends PureComponent {
  constructor(props) {
    super(props);
  }

  routeTo = (route) => {
    return () => {
      const curLocation = window.location.pathname + window.location.search,
        {history} = this.props;

      ReactGA.event({
        category: "navbar",
        action: "routedTo",
        label: curLocation + ":" + route
      });

      history.push(route);
    };
  }

  render() {
    const {classes, icon, routeLocation} = this.props;

    return (
      <li className={classes}>
        <button onClick={this.routeTo(routeLocation)}>
					<i className={classNames("zmdi", icon)} />
        </button>
      </li>
    );
  }
}


export default withRouter(RouterButtonWithIcon);
