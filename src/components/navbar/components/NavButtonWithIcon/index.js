import React, {PureComponent} from "react";
import { withRouter } from "react-router-dom";
import classNames from "classnames";
import ReactGA from "react-ga";

import "./styles.scss";

class NavButtonWithIcon extends PureComponent {
  constructor(props) {
    super(props);
  }

  handleClick = () => {
    this.props.onClick();
  }

  render() {
    const {classes, icon} = this.props;

    return (
      <li className={classes}>
        <button onClick={this.handleClick}>
					<i className={classNames("zmdi", icon)} />
        </button>
      </li>
    );
  }
}


export default withRouter(NavButtonWithIcon);
