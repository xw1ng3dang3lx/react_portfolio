import React, {PureComponent} from "react";
import { withRouter } from "react-router-dom";

import ReactGA from "react-ga";

class RouterButton extends PureComponent {
  constructor(props) {
    super(props);
  }

  routeTo = (route) => {
    return () => {
      const curLocation = window.location.pathname + window.location.search,
        {history} = this.props;

      ReactGA.event({
        category: "navbar",
        action: "routedTo",
        label: curLocation + ":" + route
      });

      history.push(route);
    };
  }

  render() {
    const {classes, history, routeLocation, label} = this.props;

    return (
      <li className={classes}>
        <button onClick={this.routeTo(routeLocation)}>
          {label}
        </button>
      </li>
    );
  }
}

RouterButton.defaultProps = {
    classes: ""
};

export default withRouter(RouterButton);
