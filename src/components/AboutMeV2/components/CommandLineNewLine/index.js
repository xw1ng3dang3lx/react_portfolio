import React, {Component} from "react";

/* CSS */
import "./styles.scss";

class CommandLineNewLine extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showBlob: true
        }   ;
    }

    componentDidMount = () => {
        this.toggleBlob();
    }

    componentWillUnmount = () => {
        if (this.blobInterval) {
            clearInterval(this.blobInterval);
        }
    }

    toggleBlob = () => {
        const setBlob = () => {
            this.setState({
                showBlob: !this.state.showBlob
            });
        };
        this.blobInterval = setInterval(setBlob, 750);
    }

    render = () => {
        return (
            <div className="command-line">
                <div className="command">
                    {">"}
                    {this.state.showBlob &&
                        <div className="rectangle-blob"></div>
                    }
                </div>
            </div>
        );
    }
}
export default CommandLineNewLine;
