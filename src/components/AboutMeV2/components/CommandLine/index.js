import React, {Component} from "react";

/* CSS */
import "./styles.scss";

class CommandLine extends Component {
    constructor(props) {
        super(props);
    }

    render = () => {
        const {listOfRespones} = this.props,
            list = listOfRespones.map((response, index) => {
                return (
                    <span key={index}>
                        {response}
                        <br />
                    </span>
                );
            });

        return (
            <div className="command-line">
                <div className="command">
                    {">"} {this.props.command}
                </div>
                <div className="response">
                    {list}
                </div>
            </div>
        );
    }
};

export default CommandLine;
