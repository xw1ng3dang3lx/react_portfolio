import React, {Component} from "react";
import { Link } from "react-router-dom";

import BigTitle from "../common/BigTitle/";
import ResumeLink from "../ResumeLink";
import CommandLine from "./components/CommandLine";
import CommandLineNewLine from "./components/CommandLineNewLine";

import classNames from "classnames";
import LazyLoad from "react-lazyload";
import Waypoint from "react-waypoint";

import {connect} from "react-redux";

/* CSS */
import "./styles.scss";
class AboutMeV2 extends Component {
    constructor(props) {
        super(props);
    }

    render = () => {
        const {nls} = this.props;
        return (
            <div className="col-100 centerContainer terminal-container">
                <div className="col-80 centeredCol">
                    <BigTitle
                        center={true}
                        title={nls.aboutMeTitle}
                    />
                    <div className="terminal">
                        <CommandLine
                            listOfRespones={[nls.aboutMeSummarySkills0, nls.aboutMeSummarySkills1, nls.aboutMeSummarySkills2, nls.aboutMeSummarySkills3, nls.aboutMeSummarySkills4]}
                            command={nls.aboutMeSummarySection}
                        />

                        <CommandLine
                            listOfRespones={[nls.aboutMeUCSD, nls.aboutMeBachelors, nls.aboutMeGPA]}
                            command={nls.aboutMeEducationSection}
                        />

                        <CommandLine
                            command={nls.aboutMeInterestsSection}
                            listOfRespones={[nls.photography, nls.aboutMeIOS, nls.aboutMeRockClimbing, nls.aboutMeMusicProduction, nls.aboutMeTurntablism]}
                        />
                        <CommandLineNewLine />
                    </div>
                </div>

                <ResumeLink />

                <div className="divider"/>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        nls: state.langReducer.nls,
        windowState: state.windowReducer
    };
}

export default connect(mapStateToProps, null)(AboutMeV2);
