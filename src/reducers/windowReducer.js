import * as types from "../constants/ActionTypes";

const initialState = {
  screenWidth: typeof window === "object" ? window.innerWidth : null,
  screenHeight: typeof window === "object" ? window.innerHeight : null
}

export default function uiReducer(state = initialState, action) {
  switch (action.type) {
    case types.SCREEN_RESIZE:
      return {
        ...state,
        screenWidth: action.screenWidth,
        screenHeight: action.screenHeight
      }
  }
  return state;
}
