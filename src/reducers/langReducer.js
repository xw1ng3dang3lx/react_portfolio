import * as types from "../constants/ActionTypes";
import {getText} from "../resources";

const initialState = {
  lang: "en",
  nls: getText("en")
}

export default function projects(state = initialState, action) {
  switch (action.type) {
    case types.SET_LANG:
      return {
        ...state,
        lang: action.lang,
        nls: getText(action.lang)
      };

    default:
      return state;
  }
};
