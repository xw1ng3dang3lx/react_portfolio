export const ADD_TODO = "ADD_TODO"
export const GET_TODOS = "GET_TODOS"
export const RECEIVE_POSTS = "RECEIVE_POSTS"
export const REQUEST_POSTS = "REQUEST_POSTS"

/* Window Reducer */
export const SCREEN_RESIZE = "SCREEN_RESIZE";

/* i18n */
export const SET_LANG = "SET_LANG";
