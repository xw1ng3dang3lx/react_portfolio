import React, {Component} from "react";
import NavBar from "./components/Navbar";
import Footer from "./components/Footer/";
import ProjectPage from "./components/ProjectPage/";
import Portfolio from "./components/";

/* Router */
import { BrowserRouter, Link, Route } from 'react-router-dom';

import {connect} from "react-redux";
import {setLang} from "./redux_actions/langActions";

import ReactGA from "react-ga";
import {configs} from "./configs";

import "./css/style.scss";

class App extends Component {
  constructor(props) {
    super(props);

    this.startTime = new Date();
    const {gaToken} = configs;
    ReactGA.initialize(gaToken);
  }

  componentDidMount = () => {
      const {setLang} = this.props;
      setLang();

      ReactGA.timing({
        category: 'Mount Time',
        variable: 'load',
        value: new Date() - this.startTime,
        label: window.location.pathname + window.location.search,
      });
  }

  render() {
    return (
      <BrowserRouter>
        <div>
          <NavBar />
          <div style={{"marginTop": "48px"}}>
            <Route path="/" exact component={Portfolio}/>
            <Route path="/timeline" component={Portfolio}></Route>
            <Route path="/projects" component={Portfolio}></Route>
            <Route path="/skills" component={Portfolio}></Route>
            <Route path="/project/:pid" component={ProjectPage}></Route>
          </div>
          <Footer />
        </div>
      </BrowserRouter>
    );
  }
}
const mapStateToProps = (state) => {
  return {};
}

const mapDispatchToProps = (dispatch => {
  return {
      setLang: () => {
          dispatch(setLang())
      }
  }
});

export default connect(mapStateToProps,mapDispatchToProps)(App);
