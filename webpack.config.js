const HtmlWebPackPlugin = require("html-webpack-plugin"),
  CopyWebpackPlugin = require("copy-webpack-plugin"),
  CleanWebpackPlugin = require("clean-webpack-plugin");
  path = require("path"),
  webpack = require('webpack');

const config = {
  entry: {
    app: [__dirname +  "/src/index.js"]
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: [
              "@babel/preset-env",
              "@babel/react"
            ],
            plugins: [
              "@babel/plugin-proposal-class-properties",
              "@babel/plugin-proposal-export-namespace-from",
              "@babel/plugin-proposal-export-default-from"
            ]
          }
        }
      },
      {
          test: /\.scss$/,
          loaders: [
              "style-loader",
              "css-loader",
              "sass-loader"
          ]
      },
      {test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: "file-loader?mimetype=image/svg+xml"},
      {test: /\.woff(\?v=\d+\.\d+\.\d+)?$/, loader: "file-loader?mimetype=application/font-woff"},
      {test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/, loader: "file-loader?mimetype=application/font-woff"},
      {test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: "file-loader?mimetype=application/octet-stream"},
      {test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: "file-loader"}
    ]
  },

  plugins: [
    new CleanWebpackPlugin(['dist']),
    new CopyWebpackPlugin([ { from: 'images', to: 'images' } ]),
    new CopyWebpackPlugin([ { from: 'movies', to: 'movies' } ]),
    new HtmlWebPackPlugin({
      template: "./src/index.html",
    }),
    new webpack.HotModuleReplacementPlugin()
  ],

  optimization: {
    splitChunks: {
      chunks: 'all'
    }
  },

  output: {
      path: path.resolve(__dirname + '/dist'),
      filename: 'bundle.js',
      publicPath: '/'
    }
};

module.exports = config;
